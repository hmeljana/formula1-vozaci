//MODULI-PLUGINOVI...
var gulp = require('gulp');
//Modul-plugin za minifikaciju koda - gulp-uglify
var uglify = require('gulp-uglify');
//Modul za osvezavanje stranice
var livereload = require('gulp-livereload');


//ngAnnotate
var ngAnnotate= require('gulp-ng-annotate');
//Bytediff
var bytediff = require('gulp-bytediff');
//Modul za konkatenaciju
var concat = require('gulp-concat');
// //Modul za minifikaciju CSS-a
var minifyCss = require('gulp-minify-css');

//Modul za greske
var plumber = require('gulp-plumber');



//task scripts
gulp.task('scripts',function(){
    console.log('starting scripts task');
    return gulp.src('js/*.js')
                .pipe(plumber(function(err){
                console.log('Styles Task Error');
                console.log(err);
                this.emit('end');
                }))
                .pipe(concat('index.js'))
                .pipe(ngAnnotate({add:true}))
                .pipe(bytediff.start())
                .pipe(uglify())   
                .pipe(bytediff.stop())  
                .pipe(gulp.dest('./dist'))
                .pipe(livereload()); 
});

gulp.task('styles',function(){
    console.log('starting styles task');   
    return gulp.src('./css/style.css')
                .pipe(plumber(function(err){
                    console.log('Styles Task Error');
                    console.log(err);
                    this.emit('end');
                }))
                // .pipe(autoprefikser({
                //     browsers:['last 2 versions','ie 8']
                // }))
                .pipe(concat('style.css'))
                .pipe(minifyCss())
                .pipe(gulp.dest('./dist'))
                .pipe(livereload()); 
});
