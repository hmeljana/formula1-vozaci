(function () {

  var app = angular
    //injektujemo modul servis1
    .module("servis1", []);

  //ne stavljamo input odnosno parametar u zagradu function-a
  app.factory('colorServis', function () {
    return {
      returnColor: function (input) {


        switch (input) {
          case '1':
            return 'first';
          case '2':
            return 'second';
          case '3':
            return 'third';
          case '4':
            return 'fourth';
          case '5':
            return 'fifth';
          case '6':
            return 'sixth';
          case '7':
            return 'seventh';
          case '8':
            return 'eighth';
          case '9':
            return 'nineth';
          case '10':
            return 'tenth';
          default:
            return 'other';

        }
      }
    }
  });

  app.factory('colorServis1', function () {
    return {
      returnColor1: function (input) {

        if (input.position === '1') {
          return 'first';
        } else if (input.position === '2') {
          return 'second';
        } else if (input.position === '3') {
          return 'third';
        } else if (input.points === '0') {
          return 'other'
        } else {
          return 'fourth'
        }

      }
    }
  });

  app.factory('colorServis2', function () {
    return {
      bestTime: function (qresult) {

        var pointsNumber = parseInt(qresult.position);
        console.log(pointsNumber);

        if (pointsNumber >= 0 && pointsNumber <= 10) {
          return qresult.Q3;
        } else if (pointsNumber > 10 && pointsNumber <= 16) {
          return qresult.Q2;
        } else {
          return qresult.Q1;
        }

      }
    }
  });


})();