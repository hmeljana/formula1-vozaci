/////IIFE

(function () {

  var app = angular.module("myModule",
    ["ngRoute",
      "myModule.js",
      'servis1'
    ]);
  app.config(function ($routeProvider) {
    $routeProvider
      .when("/drivers", {
        templateUrl: "templates/drivers.html",
        controller: "driversCtrl"
      })
      .when("/drivers/:id", {

        templateUrl: "templates/driver.html",
        controller: "driverCtrl"
      })
      .when("/teams", {
        templateUrl: "templates/teams.html",
        controller: "teamsCtrl"
      })
      .when("/teams/:id", {
        templateUrl: "templates/team.html",
        controller: "teamCtrl"
      })
      .when("/races", {
        templateUrl: "templates/races.html",
        controller: "racesCtrl"
      })
      .when("/races/:id", {
        templateUrl: "templates/race.html",
        controller: "raceCtrl"
      })
      .otherwise({
        redirectTo: "/drivers"
      })
  });

  
  app.controller("driversCtrl", function ($scope, $http, $rootScope ) {
    $scope.msg = "Drivers Championship";
    // let season;

    var button = document.getElementById("chooseYear");
    button.addEventListener("click", chooseYear);
     
  
    if(localStorage.getItem("year") ==""){
      address= "http://ergast.com/api/f1/2013/driverstandings.json";     
    }
    else {
      address = "http://ergast.com/api/f1/" + localStorage.getItem("year") +"/driverstandings.json";
      
    }

    $http.get(address)
    .then(function(response){
      $scope.content = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;
            $scope.sezona = response.data.MRData.StandingsTable.season;
    });


    function chooseYear(){
        season = document.querySelector("#choosenYear").value;
        localStorage.setItem("year", season);
        if(season ==""){
          season = "2013";
          address = "http://ergast.com/api/f1/" + season + "/driverstandings.json";
        }
        else{
          address = "http://ergast.com/api/f1/" + localStorage.getItem("year") + "/driverstandings.json";

        }

        $http.get(address)
        .then(function(response){
            $scope.content = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;
            $scope.sezona = response.data.MRData.StandingsTable.season;
            // console.log($scope.drivers);
        })
       
      
    }
    $scope.searchFilter = function (x) {

      var keyword = new RegExp($scope.nameFilter, "i");
      return !$scope.nameFilter || keyword.test(x.Driver.givenName) || keyword.test(x.Driver.familyName);
    }
});


  app.controller("driverCtrl", function ($scope, $routeParams, $http, colorServis, $rootScope) {

    var id = $routeParams.id;
    $scope.races = [];

    console.log(localStorage.getItem("year"));
    $http({
        params: {
          id: $routeParams.id
        },
        url: "http://ergast.com/api/f1/"+ localStorage.getItem("year")+"/drivers/" + id + "/driverStandings.json",
        method: "get"

      })
      .then(function (response) {

        // console.log($rootScope.year);
        //console.log($scope.content);

        $scope.x = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings[0];
        console.log($scope.x);

      }, function (response) {
        // Second function handles error
        $scope.content = "Something went wrong";
      });

    $http({
      params: {
        id: $routeParams.id
      },
      url: "http://ergast.com/api/f1/"+ localStorage.getItem("year") +"/drivers/" + id + "/results.json",
      method: "get"

    }).then(function (response) {

      // console.log($scope.races);

      $scope.races = response.data.MRData.RaceTable.Races;
      console.log($scope.races);

    }, function (response) {
      // Second function handles error
       $scope.content = "Something went wrong";
    });

    $scope.getClass = function (input) {
      return colorServis.returnColor(input);
    }

  });

  app.controller("teamsCtrl", function ($scope, $http, $rootScope) {
    $scope.msg = "Constructors Championships";
    console.log(localStorage.getItem('year'));

    var address;

    if(localStorage.getItem("year") ==""){
      address= "http://ergast.com/api/f1/2013/constructorStandings.json";      
    }
    else {
      address = "http://ergast.com/api/f1/" + localStorage.getItem("year") +"/constructorStandings.json";    
    }
    
    $http.get(address)
    .then(function(response){
      $scope.teams = response.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings;
      $scope.sezona = response.data.MRData.StandingsTable.season;
    }); 

    var season;
    var button = document.getElementById("chooseYear");

    button.addEventListener("click", chooseYear);
    function chooseYear(){
      season = document.querySelector("#choosenYear").value;
      localStorage.setItem("year", season);
      if(localStorage.getItem('year') ==""){
        season = "2013";
        address = "http://ergast.com/api/f1/" + season +"/constructorStandings.json";
      }
      else{
        address = "http://ergast.com/api/f1/" +localStorage.getItem('year') +"/constructorStandings.json";

      }
   
    $http.get(address)
    .then(function(response){
      $scope.teams = response.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings;
      $scope.sezona = response.data.MRData.StandingsTable.season;
    });
  }


    $scope.searchFilter = function (team) {

      var keyword = new RegExp($scope.teamFilter, "i");
      return !$scope.teamFilter || keyword.test(team.Constructor.name);
    }

  

});

  app.controller("teamCtrl", function ($scope, $routeParams, $http, colorServis, $rootScope) {

    var id = $routeParams.id;

    // console.log(id);
    $http({
        params: {
          id: $routeParams.id
        },
        url: "http://ergast.com/api/f1/"+ localStorage.getItem("year") +"/constructors/" + id + "/constructorStandings.json",
        method: "get"

      })
      .then(function (response) {
        // console.log(response);
        //console.log(response.data.MRData.StandingsTable.StandingsLists[0]);
        $scope.team = response.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings[0];

        console.log($scope.team);

      }, function (response) {
        // Second function handles error
        $scope.teams = "Something went wrong";
      });

    $http({
        params: {
          id: $routeParams.id
        },
        url: "http://ergast.com/api/f1/"+ localStorage.getItem("year") +"/constructors/" + id + "/results.json",
        method: "get"

      })
      .then(function (response) {
        console.log(response);
        //console.log(response.data.MRData.StandingsTable.StandingsLists[0]);
        $scope.competRaces = response.data.MRData.RaceTable.Races;
        $scope.results = response.data.MRData.RaceTable.Races[0].Results;
        console.log($scope.competRaces);
         console.log($scope.results);

      }, function (response) {
        // Second function handles error
        $scope.teams = "Something went wrong";
      });


      $scope.getClass = function (input) {
        return colorServis.returnColor(input);
      }
  });

  app.controller("racesCtrl", function ($scope, $http, $rootScope) {
   
    $scope.msg = "Race Calendar";
    var season;
    var button = document.getElementById("chooseYear");

    button.addEventListener("click", chooseYear);
 
    var address;
    
    if(localStorage.getItem("year") ===""){
      season ="2013";
      address= "http://ergast.com/api/f1/2013/results/1.json"; 
      
    }
    else {
      season = localStorage.getItem("year");
      address = "http://ergast.com/api/f1/"+ localStorage.getItem("year") +"/results/1.json";
      
    }

    $http.get(address)
    .then(function(response){
      $scope.races = response.data.MRData.RaceTable.Races;
      $scope.sezona = season;
      // console.log(sezona);
    });


    function chooseYear(){
        season = document.querySelector("#choosenYear").value;
        localStorage.setItem("year", season);
        if(season ==""){
          season = "2013";
          address = "http://ergast.com/api/f1/"+ season +"/results/1.json";
        }
        else{
          address = "http://ergast.com/api/f1/" + localStorage.getItem("year") + "/results/1.json";

        }

        $http.get(address)
        .then(function(response){
          $scope.races = response.data.MRData.RaceTable.Races;
          $scope.sezona = season;
            // console.log($scope.drivers);
        })
      }

    $scope.searchFilter = function (race) {

      var keyword = new RegExp($scope.raceFilter, "i");
      return !$scope.raceFilter || keyword.test(race.raceName);
    }
  });

  app.controller("raceCtrl", function ($scope, $routeParams, $http,colorServis1, colorServis2) {

    var id = $routeParams.id;
    console.log(id);
    $http({
        url: "http://ergast.com/api/f1/"+ localStorage.getItem("year")+"/" + id + "/qualifying.json",
        params: {
          id: $routeParams.id
        },

        method: "get"

      })
      .then(function (response) {
        // console.log(response);
        $scope.race = response.data.MRData.RaceTable.Races[0];
        console.log($scope.race);    
        $scope.qResults = response.data.MRData.RaceTable.Races[0].QualifyingResults;
        //  console.log($scope.qResults);

      }, function (response) {
        // Second function handles error
        $scope.race = "Something went wrong";
      });


    $http({
        url: "http://ergast.com/api/f1/"+ localStorage.getItem("year") + "/" + id + "/results.json",
        params: {
          id: $routeParams.id
        },

        method: "get"

      })
      .then(function (response) {
        console.log(response);

        $scope.results = response.data.MRData.RaceTable.Races[0].Results;
        console.log($scope.results);

      }, function (response) {
        // Second function handles error
        $scope.race = "Something went wrong";
      });


    $scope.bestTime = function (qresult) {
       return colorServis2.bestTime(qresult);

      // var pointsNumber = parseInt(qresult.position);
      //   console.log(pointsNumber);

      //   if (pointsNumber >= 0 && pointsNumber <= 10) {
      //     return qresult.Q3;
      //   } else if (pointsNumber > 10 && pointsNumber <= 16) {
      //     return qresult.Q2;
      //   } else {
      //     return qresult.Q1;
      //   }
    }

    $scope.getClass1 = function (input) {
      return colorServis1.returnColor1(input);
    }

  })


})();


// ergastAPIservice.getDriverRaces($scope.id).then(function (response) {
//     $scope.races = response.MRData.RaceTable.Races; 
//     console.log(races);
// });
// app.config(function ($sceDelegateProvider) {
//   $sceDelegateProvider.resourceUrlWhitelist([
//     // Allow same origin resource loads.
//     'self',
//     // Allow loading from our assets domain. **.
//     'http://ergast.com/api/f1/2015/driverStandings.json'
//   ])
// });

// (function () {



//   app.controller("driversCtrl", function ($scope, ergastAPIservice, $http) {
//     $scope.nameFilter = null;
//     $scope.driversList = [];
//     $scope.searchFilter = function(driver) {
//       var keyword = new RegExp($scope.nameFilter, "i");
//       return !$scope.nameFilter || keyword.test(driver.driver.givenName) || keyword.test(driver.driver.familyName);
//     }

//     ergastAPIservice.getDrivers().success(function (response) {
//       $scope.driversList = response.MRData.StandingsTable.StandingsLists[0].DriverStandings;
//   });

// });



  // app.controller("mainCtrl", function($scope, $http){


  // });
  // app.controller("driversCtrl", function ($scope, $http, $rootScope ) {
  //   $scope.msg = "Drivers Championship";
    
  //    $rootScope.years = ["2019","2018","2017","2016","2015","2014","2013","2012","2011","2010"];
    
  //     // var Year=$scope.selectedYear;
  //   //  $http.get("http://ergast.com/api/f1/+Year+/driverStandings.json")
   
  //  $scope.get = function(selectedYear){
  //    $rootScope.year = selectedYear;
   
  //   $http.get("http://ergast.com/api/f1/" + $rootScope.year +"/driverStandings.json")
  //   .then(function (response) {
  //     // console.log(response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings);
  //     $scope.content = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;

  //   }, function (response) {
  //     // Second function handles error
  //     $scope.content = "Something went wrong";
  //   });
  //  }
   

  //   $scope.searchFilter = function (x) {

  //     let keyword = new RegExp($scope.nameFilter, "i");
  //     return !$scope.nameFilter || keyword.test(x.Driver.givenName) || keyword.test(x.Driver.familyName);
  //   }
    
  // })

   // console.log(season);
        // $http.get("http://ergast.com/api/f1/" + localStorage.getItem("year") + "/driverstandings.json")
        // .then(function(response){
        //     $scope.content = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;
        //     $scope.sezona = response.data.MRData.StandingsTable.season;
        //     // console.log($scope.drivers);
        // })

        
    // $scope.get1 = function(selectedYear){
    //   $rootScope.year = selectedYear;
    // }
    // console.log($rootScope.year);
    // var season;
    // $scope.reloadData = function($route) {
    //     $route.reload();
    // }

    
      // $http.get(address)
      // .then(function(response){
      //      $scope.teams = response.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings;
      //     $scope.sezona = response.data.MRData.StandingsTable.season;
         
      // })

    // function chooseYear(){
    //   season = document.querySelector("#choosenYear").value;
    //   localStorage.setItem("year", season);
    //   if(season == ""){
    //     season = "2013";
    //     address = "http://ergast.com/api/f1/"+ localStorage.getItem("year") +"/constructorStandings.json";
    //   }
    //   else{
    //     address = "http://ergast.com/api/f1/"+ localStorage.getItem("year") +"/constructorStandings.json";
    //   }

    // $http.get("http://ergast.com/api/f1/"+ localStorage.getItem("year") +"/constructorStandings.json")
    //   .then(function (response) {
    //     // console.log(response.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings);
    //     $scope.teams = response.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings;
    //     $scope.sezona = response.data.MRData.StandingsTable.season;
    //   }, function (response) {
    //     // Second function handles error
    //     $scope.teams = "Something went wrong";
    //   });
    // }


    
    // function chooseYear(){
    //   season = document.querySelector("#choosenYear").value;
    //   localStorage.setItem("year", season);
    // $http.get("http://ergast.com/api/f1/"+ localStorage.getItem("year") +"/results/1.json")
    //   .then(function (response) {
    //     // console.log(response.data.MRData.RaceTable.Races[0]);
    //     // console.log(response.data.MRData.RaceTable.Races[0].raceName);

    //     $scope.races = response.data.MRData.RaceTable.Races;
    //     $scope.sezona = response.data.MRData.StandingsTable.season;
    //     // console.log(races);
    //   }, function (response) {
    //     // Second function handles error
    //     $scope.races = "Something went wrong";
    //   });
    // }
