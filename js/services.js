(function (){
 

angular.module('myModule.js', []).
  factory('ergastAPIservice', function($http, $sce, $scope) {

    var ergastAPI = {};

    ergastAPI.getDrivers = function() {
      return $http({
        method: 'JSONP', 
        url: 'http://ergast.com/api/f1/2015/driverStandings.json?callback=JSON_CALLBACK'
      });
    }

    ergastAPI.getDriverDetails = function(id) {

      // $scope.trustSrc = function(src) {
      //   return $sce.trustAsResourceUrl(src);
      // }
    
      // $scope.movie = {src:"http://www.youtube.com/embed/Lx7ycjC8qjE", title:"Egghead.io AngularJS Binding"};
      $scope.url = "http://ergast.com/api/f1/2015/drivers/'+ id +'/driverStandings.json?callback=JSON_CALLBACK";
      $scope.trusted = function(url){
        $scope.nesto =  $sce.trustAsResourceUrl(url);
        return $http({
          // params:{id:$routeParams.id},
          method: 'JSONP', 
          url: $scope.nesto
        });
      }

      }
      //   return $http({
      //     // params:{id:$routeParams.id},
      //     method: 'JSONP', 
      //     url: 'http://ergast.com/api/f1/2015/drivers/'+ id +'/driverStandings.json?callback=JSON_CALLBACK'
      //   });
      // }

    ergastAPI.getDriverRaces = function(id) {
        return $http({
          method: 'JSONP', 
          url: 'http://ergast.com/api/f1/2015/drivers/'+ id +'/results.json?callback=JSON_CALLBACK'
        });
      }

    return ergastAPI;
  });



} ());