/////IIFE

(function () {

  var app = angular.module("myModule",
    ["ngRoute",
      "myModule.js",
      'ngSanitize']);
  app.config(function ($routeProvider) {
    $routeProvider
      .when("/drivers", {
        templateUrl: "templates/drivers.html",
        controller: "driversCtrl"
      })
      .when("/drivers/:id", {

        templateUrl: "templates/driver.html",
        controller: "driverCtrl"
      })
      .when("/teams", {
        templateUrl: "templates/teams.html",
        controller: "teamsCtrl"
      })
      .when("/teams/:id", {
        templateUrl: "templates/team.html",
        controller: "teamCtrl"
      })
      .when("/races", {
        templateUrl: "templates/races.html",
        controller: "racesCtrl"
      });
  });
 

  app.controller("driversCtrl", function ($scope, $http) {
    $scope.msg = "Drivers Championship";
    $http.get("http://ergast.com/api/f1/2015/driverStandings.json")
      .then(function (response) {
        // console.log(response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings);
        $scope.content = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;

      }, function (response) {
        // Second function handles error
        $scope.content = "Something went wrong";
      });
  })

  app.controller("driverCtrl", function ($scope, $routeParams, $http) {

    var id = $routeParams.id;

    $http({
      params: { id: $routeParams.id },
      url:  "http://ergast.com/api/f1/2015/drivers/"+ id +"/driverStandings.json",
      method: "get"

    })
      .then(function (response) {
      
          console.log($scope.content);

         $scope.x = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings[0];
         console.log($scope.x);
      }, function (response) {
        // Second function handles error
        $scope.content = "Something went wrong";
      });
  });

  app.controller("racesCtrl", function ($scope, $http) {
    $scope.msg = "Race Calendar";
    $http.get("http://ergast.com/api/f1/2019/results/1.json")
    .then(function (response) {
      console.log(response.data.MRData.RaceTable.Races[0].raceName);
     
      $scope.races = response.data.MRData.RaceTable.Races;
// console.log(races);
    }, function (response) {
      // Second function handles error
      $scope.races = "Something went wrong";
    });

  });


  app.controller("teamsCtrl", function ($scope, $http) {
    $scope.msg = "Constructors Championships";

    $http.get("http://ergast.com/api/f1/2015/constructorStandings.json")
      .then(function (response) {
        // console.log(response.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings);
        $scope.teams = response.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings;

      }, function (response) {
        // Second function handles error
        $scope.teams = "Something went wrong";
      });

  });

  app.controller("teamCtrl", function ($scope, $routeParams, $http) {
   
    var id = $routeParams.id;

    // console.log(id);
    $http({
      params: { id: $routeParams.id},
      url: "http://ergast.com/api/f1/2015/constructors/" + id + "/constructorStandings.json",
      method: "get"

    })
      .then(function (response) {
        // console.log(response);
        //console.log(response.data.MRData.StandingsTable.StandingsLists[0]);
        $scope.team = response.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings[0];
       
        // console.log($scope.team);
        

      }, function (response) {
        // Second function handles error
        $scope.teams = "Something went wrong";
      });

  });

})();


    // ergastAPIservice.getDriverRaces($scope.id).then(function (response) {
    //     $scope.races = response.MRData.RaceTable.Races; 
    //     console.log(races);
    // });
 // app.config(function ($sceDelegateProvider) {
  //   $sceDelegateProvider.resourceUrlWhitelist([
  //     // Allow same origin resource loads.
  //     'self',
  //     // Allow loading from our assets domain. **.
  //     'http://ergast.com/api/f1/2015/driverStandings.json'
  //   ])
  // });

// (function () {

//   var app = angular.module("myModule",
//                   ["ngRoute",
//                   "myModule.js"]);

//   app.config(function ($routeProvider) {
//     $routeProvider
//       .when("/drivers", {
//         templateUrl: "templates/drivers.html",
//         controller: "driversCtrl"
//       })
//       .when("/drivers/:id", {
//         templateUrl: "templates/drivers.html",
//         controller: "driversCtrl"
//       })

//       .when("/teams", {
//         templateUrl: "templates/teams.html",
//         controller: "teamsCtrl"
//       })
//       .when("/races", {
//         templateUrl: "templates/races.html",
//         controller: "racesCtrl"
//       })
//       .otherwise({
//          redirectedTo: '/drivers' });
//   });

//   app.controller("driversCtrl", function ($scope, ergastAPIservice, $http) {
//     $scope.nameFilter = null;
//     $scope.driversList = [];
//     $scope.searchFilter = function(driver) {
//       var keyword = new RegExp($scope.nameFilter, "i");
//       return !$scope.nameFilter || keyword.test(driver.driver.givenName) || keyword.test(driver.driver.familyName);
//     }

//     ergastAPIservice.getDrivers().success(function (response) {
//       $scope.driversList = response.MRData.StandingsTable.StandingsLists[0].DriverStandings;
//   });

// });

//    // $http.get("drivers.html")
//      // .then(function (response) {
//         // First function handles success
//        // $scope.content = response.data;
//      // }, function (response) {
//         // Second function handles error
//       //  $scope.content = "Something went wrong";
//      // });



//   app.controller("teamsCtrl", function ($scope) {
//     $scope.msg = "I love London";
//   });
//   app.controller("racesCtrl", function ($scope) {
//     $scope.msg = "I love Paris";
//   });

// }());


